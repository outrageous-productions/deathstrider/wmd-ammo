WMD Ammo, a powerful "I just want to kill this now" ammo addon for Deathstrider.

Includes five types of murderous munitions:

- 5.56mm Micro Missiles: Baby explosives seeking point of contact. To be unloaded rapidly with a Ripper or Venom.
- 4 Gauge: Heavy duty projectiles to be launched from a shotgun. Like slugging the opposition with several lead baseball bats.
- .50 BMG HEAP: Penetration with action movie style. Each target penetrated also explodes.
- Doomsday Rockets: A deadly MIRV-style rocket. Rocket mode explodes into several homing rockets, grenade mode carpet bombs the area.
- 35mm Railshot: 35mm rips through targets. 35mm Railshot rips through any nearby friends of theirs with the force of a sonic boom.